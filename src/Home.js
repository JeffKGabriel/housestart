import React, { Component } from "react";
import { initializeApp } from "firebase/app";
//import { getFirestore, collection, getDocs, doc, getDoc } from "firebase/firestore"
import Axios from "axios";
//import { getDatabase, ref, onValue, child, get } from "firebase/database";
import Switch from "react-input-switch";
import { withCookies, Cookies } from "react-cookie";
import Invite from "./home/Invite";
import Header from "./home/Header";
import Groups from "./home/Groups";

import {
  Connection, // connection receives an endpoint argument containing the full node's URL.
  clusterApiUrl,
  Keypair,
  LAMPORTS_PER_SOL,
} from "@solana/web3.js";
//import * as bip39 from "bip39";

class Home extends Component {
  constructor(props) {
    super(props);

    let { cookies } = props;

    let cookiesAll = cookies.getAll();
    let cookieArr = Object.values(cookiesAll);

    this.state = {
      darkMode: false, //true,
      bodyHeight: window.innerWidth,
      bodyWidth: window.innerHeight,
      keypair: null,
      myConnect: null,
      walletConnected: true,
    };
    this.myConnect = null;
  }

  componentDidMount() {
    this.solanaConnect();
  }

  getBalance = async (publicKey) => {
    const lamports = await this.myConnect.getBalance(publicKey);
    return lamports;
  };

  solanaConnect = async () => {
    this.myConnect = await new Connection(clusterApiUrl("devnet")); //'devnet'

    const keypair = await Keypair.generate();
    this.setState({ keypair });

    const airdropSignature = await this.myConnect.requestAirdrop(
      keypair.publicKey,
      LAMPORTS_PER_SOL
    );
    const signature = await this.myConnect.confirmTransaction(airdropSignature);
    console.log("signature", signature);

    const newBalance = await this.getBalance(keypair.publicKey);
    console.log("newBalance", newBalance);

    //let balance = await myConnect.getBalance();
    //console.log("balance", balance);

    /*
    const mnemonic = bip39.generateMnemonic();
    console.log("Your password:", mnemonic);
    const account = this.generateAccount(mnemonic);
    console.log("Account:", account);
    */
  };
  render() {
    let { keypair, darkMode, walletConnected } = this.state;
    let { isMobile } = this.props;

    console.log("isMobile", isMobile);

    let cellWidth = 375;

    let keyPublic = keypair?.publicKey?.toBase58();

    return (
      <div
        className={darkMode ? "pageDark" : "page"}
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <Header
          keyPublic={keyPublic}
          isMobile={isMobile}
          cellWidth={cellWidth}
        />
        <div className={"mid"} style={{ marginTop: 10 }}>
          {walletConnected && <Groups cellWidth={cellWidth} />}
          {!walletConnected && (
            <Invite isMobile={isMobile} cellWidth={cellWidth} />
          )}
        </div>

        <div className={"footer"}></div>
      </div>
    );
  }
}

export default withCookies(Home);
