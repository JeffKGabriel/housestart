import React, { Component, useEffect, useCallback } from "react";
import RLDD from "react-list-drag-and-drop/lib/RLDD";

class DragDrop extends Component {
  constructor(props) {
    super(props);
    state = {
      items: [{ title: "1" }],
    };
  }

  changeItem = (newItems) => {
    this.setState({ items: newItems });
  };

  render() {
    let { items } = this.state;

    return (
      <div style={{ backgroundColor: "red" }}>
        <RLDD
          items={items}
          itemRenderer={(item) => {
            return <div>{item.title}</div>;
          }}
          onChange={this.changeItem}
        />
      </div>
    );
  }
}

export default DragDrop;
