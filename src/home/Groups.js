import React, { useState } from "react";

let Groups = ({ cellWidth }) => {
  const [openGroup, setGroupOpen] = useState("");

  let groupsArr = [
    { name: "animaniacs" },
    { name: "bobby" },
    { name: "fischer" },
  ];
  let groupList = groupsArr.map((a, k) => {
    return (
      <div
        style={{
          display: "flex",
          alignSelf: "stretch",
          flexDirection: "column",
          alignItems: "flex-end",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignSelf: "stretch",
          }}
        >
          <div
            onClick={() => setGroupOpen(a.name)}
            key={k}
            className={"textBox"}
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              flex: 1,
              marginBottom: 6,
              marginLeft: 6,
              marginRight: 6,
              height: 28,
            }}
          >
            <div>{a.name}</div>
            <div
              style={{ display: "flex", flexDirection: "row", fontSize: 16 }}
            >
              <div style={{ marginRight: 9 }}>2 new</div>
              <div>3 vote</div>
              {false && (
                <div style={{ fontSize: 11, opacity: 0 }}>
                  {false ? "▼" : "◀"}
                </div>
              )}
            </div>
          </div>
          {openGroup == a.name && (
            <div
              className={"textBox"}
              style={{
                backgroundColor: "#4ee4bf",
                color: "#fff",
                height: 28,
                width: 28,
                marginLeft: -2,
                marginRight: 4,
                fontSize: 28,
              }}
            >
              +
            </div>
          )}
        </div>
        {openGroup == a.name && <div style={{ height: 27 }}>resolve</div>}
      </div>
    );
  });
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignSelf: "stretch",
        width: cellWidth,
      }}
    >
      {groupList}
    </div>
  );
};

export default Groups;
