import React, { useState } from "react";
import Identicon from "react-identicons";

let Invite = ({ isMobile }) => {
  const [showInvite, toggleInvite] = useState(false);

  let members = ["johanson", "kylie", "rebecca", "chloe"];
  let showMembers = members.map((a, k) => {
    return (
      <div
        key={k}
        style={{
          borderRadius: 3,
          border: "1px solid #ccc",
          display: "flex",
          padding: 8,
          paddingLeft: 11,
          paddingRight: 11,
          marginBottom: 6,
        }}
      >
        {a}
      </div>
    );
  });
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <div
        className={"intro"}
        style={{
          marginTop: 16,
          alignSelf: "stretch",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          border: "1px solid #ddd",
          padding: 18,
          borderRadius: 1,
          marginBottom: 30,
        }}
      >
        <div style={{ marginBottom: 7 }}>-- social Oracle --</div>
        <div style={{ opacity: 0.9 }}>predict & resolve with friends</div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            marginBottom: 7,
          }}
        >
          <div>invite from:</div>
          <div className={"textBox"} style={{ marginLeft: 5 }}>
            <Identicon size={26} string={"cr34z"} />
            <div style={{ marginLeft: 6 }}>cruz</div>
          </div>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            marginBottom: 6,
            marginBottom: 34,
          }}
        >
          <div style={{ marginRight: 5 }}>group:</div>
          <div className={"textBox"} style={{ alignSelf: "baseline" }}>
            animaniacs
          </div>
        </div>

        <div style={{ alignSelf: "stretch" }}>
          <div>predictions are resolved by all members</div>
          <div>only join groups you trust</div>
        </div>

        <div
          style={{
            alignSelf: "stretch",
            alignItems: "flex-end",
            display: "flex",
            flexDirection: "column",
            marginTop: 40,
          }}
        >
          {showInvite ? (
            <>
              <div
                className={"textBox"}
                onClick={() => toggleInvite()}
                style={{
                  width: 140,
                  marginBottom: 6,
                }}
              >
                name
              </div>
              <div
                className={"textBox"}
                onClick={() => toggleInvite()}
                style={{
                  width: 140,
                  marginBottom: 6,
                }}
              >
                connect wallet
              </div>
              <div
                className={"textBox"}
                onClick={() => toggleInvite()}
                style={{
                  width: 140,
                }}
              >
                join
              </div>
            </>
          ) : (
            <div
              className={"textBox"}
              onClick={() => toggleInvite(true)}
              style={{
                width: 140,
                color: "#fff",
                backgroundColor: "#4ee4bf",
              }}
            >
              accept invite
            </div>
          )}
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            marginTop: 44,
            marginBottom: 30,
          }}
        >
          <div
            style={{
              marginBottom: 10,
              display: "flex",
              alignSelf: "stretch",
              border: "0px solid #ccc",
              borderBottomWidth: 1,
              justifyContent: "flex-end",
            }}
          >
            members (8)
          </div>
          {showMembers}
        </div>
      </div>
    </div>
  );
};

export default Invite;
