/*

<div
  style={{
    width: 250,
    border: darkMode ? "1px solid #555" : "1px solid #ccc",
    borderRadius: 8,
    paddingBottom: 10,
  }}
>
  <div style={{ marginTop: 12, marginBottom: 10, fontWeight: 300 }}>
    NA
  </div>
  {showTeams}
</div>



let teamArr = [
  { name: "Blak Intl", wins: 0, loss: 0 },
  { name: "Misfits", wins: 0, loss: 0 },
  { name: "TeamSMG", wins: 0, loss: 0 },
  { name: "Nigma Galaxy", wins: 0, loss: 0 },
  { name: "Team Liquid", wins: 0, loss: 0 },
  { name: "Tundra", wins: 0, loss: 0 },
  { name: "EG", wins: 0, loss: 0 },
  { name: "Four Zoomers", wins: 0, loss: 0 },
];
let showTeams = teamArr.map((a, k) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignSelf: "stretch",
        justifyContent: "space-between",
        paddingLeft: 9,
        paddingRight: 9,
        marginTop: 8,
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <div
          style={{
            height: 36,
            width: 36,
            backgroundColor: "#fff",
            borderRadius: 4,
            marginRight: 5,
            opacity: darkMode ? 0.5 : 0.8,
            border: darkMode ? "1px solid #fff" : "1px solid #ccc",
          }}
        />
        <div>{a.name}</div>
      </div>
      <div>
        {a.wins} - {a.loss}
      </div>
    </div>
  );
});



*/
