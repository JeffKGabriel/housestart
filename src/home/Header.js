import React from "react";
import Identicon from "react-identicons";

let Header = ({ isMobile, cellWidth, keyPublic }) => {
  return (
    <div className={"top"}>
      <div
        className={"topMid"}
        style={{
          width: isMobile ? cellWidth : 700,
        }}
      >
        <div style={{ marginLeft: isMobile ? 10 : 0 }}>soCo</div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <div
            style={{
              borderRadius: 2,
              border: "1px solid #ccc",
              padding: 3,
              width: 28,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              opacity: 0.6,
            }}
          >
            <Identicon size={26} string={keyPublic} />
          </div>
          <div style={{ marginLeft: 6, width: 80, overflow: "hidden" }}>
            {keyPublic}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
