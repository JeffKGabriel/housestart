var express = require('express');
var path = require('path');
var compression = require('compression')


var cors = require('cors');
var corsOptions = {
    origin: '*',
    credentials: true };



var site = express();
var port = 80;

site.use(compression())



//site.use(cors(corsOptions));
//res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');
site.use(function(req, res, next) {
        // do logging
  res.setHeader('Access-Control-Allow-Origin', 'https://bombage.xyz');
  res.setHeader('Access-Control-Allow-Credentials','true')
  //res.setHeader('Access-Control-Allow-Origin', 'http://104.131.167.44');
        //console.log('Something is happening.');
        next();
});
site.use('/', express.static(path.resolve(__dirname, 'dist')));







//serve api


var api = require('./api/index');
var router = require('./api/router');

api.use('/api', router);
api.listen(8888);
console.log("api started on port :8888");

module.exports = api;



//non ssl

/*
site.listen(port, ()=>{
  console.log('site served on port :',port);
})

var http = require('http');
var ws = require('websockets')
*/




//ssl



var http = require('http');
var https = require('https');
var fs = require('fs');

var options = {
     key: fs.readFileSync('/etc/letsencrypt/live/www.bombage.xyz/privkey.pem'),
     cert: fs.readFileSync('/etc/letsencrypt/live/www.bombage.xyz/fullchain.pem'),
     ca: fs.readFileSync('/etc/letsencrypt/live/www.bombage.xyz/chain.pem')
}

var server = https.createServer(options, site);




// this http redirect works

var redirectApp = express(),
redirectServer = http.createServer(redirectApp);

redirectApp.use(function requireHTTPS(req, res, next) {
  if (!req.secure) {
    console.log("redirect 80 to ssl : " + req.url);
    //console.log("code = " + req.query.code);
    //return res.redirect('https://' + req.headers.host + req.url);
    res.redirect('https://bombage.xyz'+ req.url);
  }
  next();
})

redirectServer.listen(80);








//const WebSocket = require('ws');



/*socket io*/

var ioApp = express();

var ioServer = http.createServer(ioApp);
//var io = new WebSocket("ws://localhost:3000");
//const io = new WebSocket.Server({ server:site });
//var ioServer = require('http').Server(ioApp);
var io = require('socket.io')(ioServer);
var userNum = 0
var lobby = []
var users = []




io.on('connection', client => {

  console.log('connected',client.id);
  userNum = userNum+1

  client.emit('welcome',{lobby,id:client.id});

  client.on('event', data => {
    console.log('event, data',data);
   });

   client.on('disconnect', (element) => {

     let id = null
     let roomId = null
     let index = -1
     for(i=0;i<lobby.length;i++){
       if(lobby[i] && lobby[i].id == client.id){
         index = i
         roomId = lobby[i].roomId
         id = lobby[i].id
       }
     }
     console.log('disconnet index',index);
     if(index > -1){
       lobby.splice(index,1)
     }
     //const index = lobby.indexOf(element);
     //lobby.splice(index, 1);
     //let playerData = checkIfWasInARoom(client.id)
         //console.log('checkIfWasInARoom',playerData);
     if(index > -1){
       io.to(`room-${roomId}`).emit('roomChat',{msg:'playerLeft',id:id})
     }
   });


   client.on('loginID',id=>{
     console.log('id',id);
   })

   client.on('refreshLobby',data=>{
     client.emit('refreshLobbyRes',{lobby});
   })

   client.on('stream',data=>{
     addStreamToLobby(data.id,data.name,data.stream,data.streamID)
     //client.join(data.id)
   })

   client.on('removeLobby',data=>{
     removeRoomFromLobby(data.roomId)
     //client.join(data.id)
   })

   client.on('joinRoom',data=>{
     console.log('joinRoom',data);
     client.join(`room-${data.roomId}`)
     addRoomToLobby(data.roomId,data.id)
     io.to(`room-${data.roomId}`).emit('roomChat',{msg:'joinedRoom',fromId:data.id})
     // client.join('test')
   })

   client.on('launchbois1',data=>{
     console.log('launchbois1');
     lobby = lobby.map(a=> a.id==data.id ? {id:a.id,stream:a.stream,streamID:a.streamID,streamData:data.data} : a )
     //client.to('test').emit('launchbois1', data);
   })

   client.on('launchbois2',data=>{
     console.log('launchbois2',data.id);
     //client.to('test').emit('launchbois2', data);
   })

   client.on('dm',data=>{
     console.log('dm',data.id);
     io.to(`${data.id}`).emit('dm',data);
     //client.to('test').emit('launchbois2', data);
   })

   client.on('roomChat',data=>{
     console.log('roomChat',data);
     io.to(`room-${data.roomId}`).emit('roomChat',{msg:data.msg,fromId:data.id,...data })
   })


});

addStreamToLobby = (id,name,stream,streamID) =>{
    lobby.push({id,name,stream,streamID})
}

addRoomToLobby=(roomId,id)=>{
  //let roomActive = lobby.filter(a=>a.roomId == roomId)
  //if(roomActive.length == 0){
    lobby.push({roomId:roomId,id:id})
  //}
}

removeRoomFromLobby=(roomId)=>{

  let newLobby = lobby.map(a=>{
    if(a.roomId != roomId){
      return a
    }
  })
  newLobby = newLobby.filter(a=>a !== undefined)

  lobby = newLobby
}

checkIfWasInARoom=(id)=>{
  //if was in a room - remove them from that room
  // and then shout to that room - the player left
  let playerIndex = null
  let roomId = null

  console.log('checkIfWasInARoom',lobby);
  lobby.map((a,k)=>{
    if(a.id == id){
      playerIndex = k
      roomId = a.roomId
    }
  })
  if(playerIndex){
    lobby.splice(playerIndex,1)
    // also tell that room that player left
    return {roomId,id}
  }
  return null

}









// serve site


site.use('/', express.static(path.resolve(__dirname, 'dist')));

server.listen(443, function () {
  console.log('site served on port :443');
})


ioServer.listen(3000, function () {
  console.log('io served on port :3000');
})







/* socket.io */

/*
const chatServer = require('http').createServer();
const io = require('socket.io')(chatServer);
*/

//var ioHttp = require('http').Server(site);
//var io = require('socket.io')(server);

//const io = new ioServer().attach(3000);


//server.listen(3000);
